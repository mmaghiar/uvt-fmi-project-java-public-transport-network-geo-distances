package ro.ptr.ggd.io;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * JSON Library URL : https://code.google.com/p/json-simple/
 * 
 * 
 * Documentation url: https://developers.google.com/maps/documentation/directions/?csw=1
 * URL : https://maps.googleapis.com/maps/api/distancematrix/json?origins=45.7504,21.2081&destinations=45.7567,21.2248&mode=driving&key=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
 * @author adi
 *
 */
public class googleRequest {
	
	public static Map<String, String> getDistance(String point1, String point2 ){
		Map<String, String> result = new HashMap<String, String>();
		
		//TODO Make call here
		try {

		    String url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=point1&destinations=point2&mode=driving&key=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";

		    url = url.replace("point1", point1);
		    url = url.replace("point2", point2);
		    
		    URL obj = new URL(url);
		    HttpURLConnection conn = (HttpURLConnection) obj.openConnection();

		    conn.setRequestProperty("Content-Type", "application/json");
		    conn.setDoOutput(true);

		    conn.setRequestMethod("GET");

		    
		    Scanner in = new Scanner(obj.openStream());
		    String resultString ="";
		    while(in.hasNext()){
		    	resultString += in.nextLine();
		    } 
		    Object jsonObj = JSONValue.parse(resultString);
		    JSONObject array = (JSONObject) jsonObj;
		    
		    JSONParser parser = new JSONParser();
		    KeyFinder finder = new KeyFinder();
		    finder.setMatchKey("text");
		    System.out.println(point1 + " " + point2);
		    try{
		      while(!finder.isEnd()){
		        parser.parse(resultString, finder, true);
		        if(finder.isFound()){
		          finder.setFound(false);
		          String value = (String) finder.getValue();
		          if(value.endsWith("km"))
		        	  result.put("distance", value.substring(0, value.indexOf(" ")));
		          else if (value.contains("min"))
		          	  result.put("duration", value.substring(0, value.indexOf(" ")));
		        }
		      }           
		    }
		    catch(ParseException pe){
		      pe.printStackTrace();
		    }
		    in.close();

		    } catch (Exception e) {
		    e.printStackTrace();
		    }
		
		return result;
	}

}
