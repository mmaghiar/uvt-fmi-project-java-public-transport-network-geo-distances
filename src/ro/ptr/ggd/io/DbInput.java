package ro.ptr.ggd.io;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

public class DbInput extends Input{

	public Connection connect;
	
	public DbInput (Connection conn){
		this.connect = conn;
	}
	
	public Connection getConnection() {
		return connect;
	}
	
	public void setConnection(Connection connection) {
		this.connect = connection;
	}
	
	public void dbQuery() throws SQLException{
		Statement sqlStatement = connect.createStatement();
		Statement insertStatement = connect.createStatement();
		ResultSet stationResultSet = sqlStatement.executeQuery("SELECT * FROM Stations ORDER by 'LineName'"); 
		
		String previousReadStation = null;
		String previousLineID = "";
		String previousLatLong = null;
		int i = 0;
		
		while(stationResultSet.next()){
			String stationName= stationResultSet.getString("RawStationName");
			String stationId = stationResultSet.getString("StationID");
			
			String LatLong = stationResultSet.getString("Lat") + "," + stationResultSet.getString("Long");
			boolean valid = !(LatLong.equals("0,0") || LatLong.equals(","));
			if(!valid)
				continue;
			String lineID = stationResultSet.getString("LineID");
			
			
			
			
			String q = "INSERT INTO `Connection`(`LineID`, `StartStationID`,"
					+ " `EndStationID`, `Distance`, `Time`) VALUES (LINEID,SSTATID,ESTATID, DIST, TIME)";
			if(previousReadStation != null && lineID.equals(previousLineID) && !previousReadStation.equals(stationId)){
				Map<String, String> values = googleRequest.getDistance(previousLatLong, LatLong);
				System.out.println("From " + previousReadStation + " to " + stationId);
				System.out.println("Duration: " + values.get("duration"));
				System.out.println("Distance: " + values.get("distance"));
				
				q = q.replace("LINEID", lineID);
				q = q.replace("SSTATID", previousReadStation);
				q = q.replace("ESTATID", stationId);
				q = q.replace("DIST", values.get("distance") != null? values.get("distance") : "0");
				q = q.replace("TIME", values.get("duration"));
						
				insertStatement.execute(q);
			}
			
			
			
			previousReadStation = stationId;
			previousLineID = lineID;
			previousLatLong = LatLong;
			i++;
		}
		System.out.println("Size: " + i);
	}
	
}
