package ro.ptr.ggd.app;

import java.sql.Connection;

import ro.ptr.ggd.controller.Controller;
import ro.ptr.ggd.db.DbManager;
import ro.ptr.ggd.io.DbInput;
import ro.ptr.ggd.io.Input;
import ro.ptr.ggd.io.Output;

/**
 * Application : Google Geo Distances
 * 
 * This application was made in order to get the distance between two stations,
 * from Google Distance Matrix API and insert it to database that helps us 
 * to create a cache for the measurements, because we are limited on Google
 * API queries.
 * 
 * @author Mihai Adrian MAGHIAR
 *
 */
public class GeoDistance {

	public static void main(String[] args) {
		
		DbManager connect = new DbManager();
		Connection connection = connect.getConnection();
		Input in = new DbInput(connection);
		Output out = new Output();
		
		Controller app = new Controller(in,out);
		app.handle();
	}

}
