package ro.ptr.ggd.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DbManager {
	Connection connection = null;
	public void closeConnection(Connection c){
		try{
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public Connection getConnection(){
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		Connection connection = null;
	 
		try {
			connection = DriverManager
			.getConnection("jdbc:mysql://projectpj.info.uvt.ro:3306/Adi_Java_PTR","ptr", "");
	 
		} catch (SQLException e1) {
			System.out.println("Connection Failed! Check output console");
			e1.printStackTrace();
		}
	 
		if (connection != null) {
			System.out.println("Connected");
		} else {
			System.out.println("Failed to connect!");
		}
	
		return connection;
	}
	
}
