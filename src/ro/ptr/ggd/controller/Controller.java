package ro.ptr.ggd.controller;

import java.sql.SQLException;

import ro.ptr.ggd.io.DbInput;
import ro.ptr.ggd.io.Input;
import ro.ptr.ggd.io.Output;

public class Controller {

	private Input input;
	private Output output;
	 
	public Controller(Input in, Output out){
		this.input = in;
		this.output = out;
	}
	
	public void handle(){
		
		DbInput dbin = (DbInput) this.input;
		try {
			dbin.dbQuery();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		output.print();
	}
}
